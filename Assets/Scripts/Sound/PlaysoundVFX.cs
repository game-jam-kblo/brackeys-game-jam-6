using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaysoundVFX : MonoBehaviour
{
    [SerializeField]
    private string nomeFMOD;

    private void OnEnable()
    {
        FMODUnity.RuntimeManager.PlayOneShot(nomeFMOD, transform.position);
    }
}
