using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControllerVCA : MonoBehaviour
{
    private Slider sliderVCA;
    private FMOD.Studio.VCA vca;
    private float volumeVCA = 1;
    public string nameVCA;

    void Start()
    {
        vca = FMODUnity.RuntimeManager.GetVCA($"vca:/{nameVCA}");
        sliderVCA = GetComponent<Slider>();
        volumeVCA = PlayerPrefs.GetFloat($"Volume{nameVCA}", volumeVCA);
        vca.getVolume(out volumeVCA);
        sliderVCA.value = volumeVCA;
    }

    public void SetVolume(float volume)
    {
        vca.setVolume(volume);
        vca.getVolume(out volumeVCA);
        PlayerPrefs.SetFloat($"Volume{nameVCA}", volumeVCA);

    }

}
