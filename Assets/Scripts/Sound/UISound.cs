using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISound : MonoBehaviour
{

    public void SelectSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/sfx_ui_button_hover");
    }

    public void ActionSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/sfx_ui_button_selection");
    }
}
