using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerSound : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player"))
            return;

        if(this.gameObject.GetComponent<Building>() != null)
        {
            float rn = Random.Range(0.25f, 0.35f);
            Soundtrack.FindObjectOfType<Soundtrack>().prediosFMOD += rn;
            FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/sfx_game_build_explosion", transform.position);
        }
            

        if (this.gameObject.GetComponent<Drop>() != null)
        {
            if (GetComponent<Drop>().type == DropType.Health)
                FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/sfx_game_cure", transform.position);
        }
    }

}
