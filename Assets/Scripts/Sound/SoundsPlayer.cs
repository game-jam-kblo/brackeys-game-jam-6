using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class SoundsPlayer : MonoBehaviour
{
    [SerializeField] private string fmodMove;
    FMOD.Studio.EventInstance eventInstance;

    private void Start()
    {
        eventInstance = FMODUnity.RuntimeManager.CreateInstance(fmodMove);
    }

    public void Fire(InputAction.CallbackContext context)
    {
        if(!GameManager.IsPaused)
            FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/sfx_game_shoot");
    }

    public void Fire2(InputAction.CallbackContext context)
    {
        if (!GameManager.IsPaused && Player.SecondShootAvailable)
            FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/sfx_tank_shoot");
    }

    public void Move(InputAction.CallbackContext context)
    {
        if (context.ReadValue<Vector2>() != Vector2.zero)
            eventInstance.start();
        else
            eventInstance.stop(0);

    }
}
