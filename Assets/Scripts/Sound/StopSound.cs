using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopSound : MonoBehaviour
{
    FMODUnity.StudioEventEmitter studioEvent;

    private void Start()
    {
        studioEvent = GetComponent<FMODUnity.StudioEventEmitter>();
    }

    void Update()
    {
        if (GameManager.IsPaused)
            studioEvent.EventInstance.setPaused(true);
        else
            studioEvent.EventInstance.setPaused(false);

    }
}
