using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soundtrack : MonoBehaviour
{
    FMODUnity.StudioEventEmitter studioEE;
    public float prediosFMOD;

    // Start is called before the first frame update
    void Start()
    {
        prediosFMOD = 0;
        studioEE = GetComponent<FMODUnity.StudioEventEmitter>();
        studioEE.Play();
    }

    private void Update()
    {
        PauseSoundtrack();
        studioEE.SetParameter("music_condition", prediosFMOD);

    }

    void PauseSoundtrack()
    {
        if (GameManager.IsPaused)
            studioEE.EventInstance.setPaused(true);
        else
            studioEE.EventInstance.setPaused(false);

    }
}
