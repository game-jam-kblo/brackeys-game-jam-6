using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadManager : MonoBehaviour
{
    public static float LoadingProgress { get; private set; }
    private static LoadManager instance;

    private void Awake()
    {
        if (instance != null)
            Destroy(gameObject);

        instance = this;
    }

    public static void LoadScene(int buildIndex)
    {
        SceneManager.LoadScene(1);
        instance.StartCoroutine(LoadSceneAsync(buildIndex));
    }

    private static IEnumerator LoadSceneAsync(int buildIndex)
    {
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(buildIndex);

        while (!asyncOperation.isDone)
        {
            LoadingProgress = Mathf.Clamp01(asyncOperation.progress / 0.9f);
            yield return null;
        }
    }
}
