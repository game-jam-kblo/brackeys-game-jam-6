using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Spawns
{
    Spawn_1,
    Spawn_2,
    Spawn_3,
    Spawn_4,
    Spawn_5,
    Spawn_6,
    Spawn_7,
    Spawn_8,
    Spawn_9,
    Spawn_10,
    Spawn_11,
    Spawn_12,
    Spawn_13,
    Spawn_14,
    Spawn_15,
    Spawn_16,
    Spawn_17,
    Spawn_18,
    Spawn_19,
    Spawn_20,
    Spawn_21,
    Spawn_22,
    Spawn_23,
    TANK,
    TANK_1,
    AMBULANCE,
    AMBULANCE_1,
    AMBULANCE_2,
    AMBULANCE_3
}

public class LevelManager : MonoBehaviour
{
    #region Static Variables

    public static List<Enemy> inactiveEnemies = new List<Enemy>();
    public static HashSet<Enemy> activeEnemies = new HashSet<Enemy>();
    public static float RemainingTime { get; private set; }
    public static int Points { get; private set; }

    #endregion

    [SerializeField] private LevelData levelData;
    [SerializeField] private Transform[] spawnPoints;

    private float startTime;

    private void Awake()
    {
        GameManager.OnResume.Invoke();
        startTime = Time.time;
        Player.SetHealth(levelData.playerHealth);
        RemainingTime = levelData.matchDuration;
        Points = 0;
        SetupEnemies();
    }

    private void Start()
    {
        StartSpawns();
    }

    private void Update()
    {
        if (GameManager.IsPaused)
            return;

        float elapsedTime = Time.time - startTime;
        RemainingTime = levelData.matchDuration - elapsedTime;

        if (RemainingTime <= 0f)
        {
            RemainingTime = 0f;
            GameOver();
        }
    }

    #region Enemies

    private void SetupEnemies()
    {
        inactiveEnemies.Clear();
        activeEnemies.Clear();
        GameObject enemiesParent = new GameObject("Enemies");
        enemiesParent.transform.SetParent(transform);

        foreach (EnemyInfo enemyInfo in levelData.enemyInfos)
            EnemyPool(enemyInfo, enemiesParent.transform);
    }

    private void EnemyPool(EnemyInfo enemyInfo, Transform parent)
    {
        for (int i = 0; i < enemyInfo.maxEnemies; i++)
        {
            GameObject go = Instantiate(enemyInfo.enemy.prefab);
            go.SetActive(false);
            go.transform.SetParent(parent);
            Enemy enemy = go.GetComponent<Enemy>();
            inactiveEnemies.Add(enemy);
        }
    }

    private void StartSpawns()
    {
        foreach (EnemyInfo enemyInfo in levelData.enemyInfos)
            StartCoroutine(Spawn(enemyInfo));
    }

    private IEnumerator Spawn(EnemyInfo enemyInfo)
    {
        yield return new WaitForSeconds(enemyInfo.startDelay);

        if (enemyInfo.endless)
        {
            while (RemainingTime > 0f)
            {
                Enemy enemy = inactiveEnemies.Find(x => x.enemyData.Equals(enemyInfo.enemy));

                if (enemy != null)
                {
                    inactiveEnemies.Remove(enemy);
                    int index = GetRandomSpawnPoint(enemyInfo);
                    enemy.transform.SetPositionAndRotation(spawnPoints[index].position, Quaternion.identity);
                    enemy.gameObject.SetActive(true);
                }

                yield return new WaitForSeconds(enemyInfo.spawnRate);
            }
        }

        else
        {
            int spawned = 0;
            while (spawned < enemyInfo.enemyAmount)
            {
                Enemy enemy = inactiveEnemies.Find(x => x.enemyData.Equals(enemyInfo.enemy));

                if (enemy != null)
                {        
                    inactiveEnemies.Remove(enemy);
                    int index = GetRandomSpawnPoint(enemyInfo);
                    enemy.transform.SetPositionAndRotation(spawnPoints[index].position, Quaternion.identity);
                    enemy.gameObject.SetActive(true);
                    spawned++;
                }

                yield return new WaitForSeconds(enemyInfo.spawnRate);
            }
        }
    }

    public int GetRandomSpawnPoint(EnemyInfo enemyInfo)
    {
        int value = Random.Range(0, enemyInfo.spawnPoints.Length);
        Spawns spawns = enemyInfo.spawnPoints[value];
        return (int)spawns;
    }

    #endregion

    #region General Methods

    public static void AddPoints(int value) => Points += value;

    private void GameOver()
    {
        GameManager.OnGameOver.Invoke();
    }

    #endregion
}

[System.Serializable]
public class EnemyInfo
{
    public EnemyData enemy;
    public float startDelay;
    public float spawnRate;

    [Tooltip("True to spawn enemies during the entire match, ignores the enemy amount")]
    public bool endless;

    [Tooltip("Total enemies to spawn during the match")]
    public int enemyAmount;

    [Tooltip("Max enemies alive")]
    public int maxEnemies;

    [Space]
    public Spawns[] spawnPoints;
}
