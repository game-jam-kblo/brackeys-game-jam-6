using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public void LoadScene(int buildIndex) => LoadManager.LoadScene(buildIndex);

    public void Pause() => GameManager.OnPause.Invoke();   

    public void Resume() => GameManager.OnResume.Invoke();

    public void Quit() => GameManager.OnQuit.Invoke();
}
