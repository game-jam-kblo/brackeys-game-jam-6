using System;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif
using UnityEngine;

public static class GameManager
{
    public static Action OnPause;
    public static Action OnResume;
    public static Action OnQuit;

    public static Action OnGameOver;

    public static bool IsPaused { get; private set; }

    [RuntimeInitializeOnLoadMethod]
    private static void Awake()
    {
        SetupSingletons();
        OnPause += Pause;
        OnResume += Resume;
        OnQuit += Quit;

        OnGameOver += GameOver;
    }

    private static void SetupSingletons()
    {
        GameObject loadManager = new GameObject("[Load Manager]");
        loadManager.AddComponent<LoadManager>();
        loadManager.hideFlags = HideFlags.NotEditable;
        GameObject.DontDestroyOnLoad(loadManager);
    }

    private static void Pause()
    {
        Time.timeScale = 0;
        IsPaused = true;
    }

    private static void Resume()
    {
        Time.timeScale = 1;
        IsPaused = false;
    }

    private static void Quit()
    {
#if UNITY_EDITOR
        EditorApplication.ExitPlaymode();
#else
        Application.Quit();
#endif
    }

    #region Gameplay

    private static void GameOver()
    {
        Pause();
    }

    #endregion

    #region Editor Methods

#if UNITY_EDITOR

    [MenuItem("Game/Play")]
    private static void PlayGame()
    {
        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        EditorSceneManager.OpenScene("Assets/Scenes/Main Menu.unity");
        EditorApplication.EnterPlaymode();
    }

#endif

    #endregion
}

