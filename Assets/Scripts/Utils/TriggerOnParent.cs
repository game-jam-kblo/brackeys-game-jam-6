using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerOnParent : MonoBehaviour
{
    [HideInInspector] public UnityEvent<Collider> enter = new UnityEvent<Collider>();

    private void OnTriggerEnter(Collider other)
    {
        enter.Invoke(other);
    }
}
