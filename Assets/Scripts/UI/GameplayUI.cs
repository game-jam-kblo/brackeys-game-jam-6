using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameplayUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI timer;
    [SerializeField] private TextMeshProUGUI points;
    [SerializeField] private Slider life;
    [SerializeField] private GameObject gameOverPanel;
    [SerializeField] private GameObject secondShootFeedback;

    private void Awake()
    {
        GameManager.OnGameOver += GameOver;       
    }

    private void Start()
    {
        life.maxValue = Player.Health;
    }

    private void OnDestroy()
    {
        GameManager.OnGameOver -= GameOver;
    }

    private void Update()
    {
        int minutes = Mathf.FloorToInt(LevelManager.RemainingTime / 60);
        int seconds = Mathf.FloorToInt(LevelManager.RemainingTime % 60);
        string timerText = string.Format("{0:00}:{1:00}", minutes, seconds);
        timer.text = string.Concat("Time left: ", timerText);
        life.value = Player.Health;

        points.text = string.Concat("Points: ", LevelManager.Points.ToString());

        secondShootFeedback.SetActive(Player.SecondShootAvailable);
    }

    private void GameOver()
    {
        gameOverPanel.SetActive(true);
    }
}
