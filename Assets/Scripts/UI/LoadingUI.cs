using UnityEngine;
using UnityEngine.UI;

public class LoadingUI : MonoBehaviour
{
    [SerializeField] private Slider slider;

    private void Update()
    {
        slider.value = LoadManager.LoadingProgress;
    }
}
