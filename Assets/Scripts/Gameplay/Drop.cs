using System.Collections;
using UnityEngine;

public enum DropType
{
    Health,
    Shield
}

public class Drop : MonoBehaviour
{
    public DropType type;
    public int value;
}
