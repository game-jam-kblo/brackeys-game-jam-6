using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleCollision : MonoBehaviour
{
    [HideInInspector] public int value;
    public ShakeSettings shakeSettings; 

    private void OnParticleCollision(GameObject other)
    {
        IDamage damage = other.GetComponentInParent<IDamage>();

        if (damage == null)
            damage = other.GetComponent<IDamage>();

        damage?.ApplyDamage(value);
        CameraShake.Shake(shakeSettings);
    }
}
