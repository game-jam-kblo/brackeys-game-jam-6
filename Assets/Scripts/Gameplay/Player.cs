using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Player : MonoBehaviour, IDamage
{
    public static int Health { get; private set; }
    public static Transform Transform { get; private set; }
    public static bool SecondShootAvailable { get; private set; }

    [SerializeField] private PlayerData playerData;
    [SerializeField] private Transform visual;

    [Header("Gun")]
    [SerializeField] private Transform lightGun;
    [SerializeField] private Transform heavyGun;
    [SerializeField] private ParticleSystem lightParticle;
    [SerializeField] private ParticleSystem heavyParticle;

    [Header("Animations")]
    [SerializeField] private Transform wheel;

    private Rigidbody rb;
    private Camera mainCamera;
    private TriggerOnParent trigger;
    private float horizontal;
    private float vertical;
    private Vector2 mousePosition;
    private Vector3 mouseWorldPosition;
    float turnSmoothVelocity;
    private float angle;

    private int points;

    private void Awake()
    {
        rb = GetComponentInChildren<Rigidbody>();
        rb.mass = playerData.mass;
        mainCamera = Camera.main;
        Transform = rb.transform;
        lightParticle.GetComponent<ParticleCollision>().value = playerData.lightFireDamage;
        heavyParticle.GetComponent<ParticleCollision>().value = playerData.heavyFireDamage;

        trigger = GetComponentInChildren<TriggerOnParent>();
        trigger.enter.AddListener(BallSlam);
        trigger.enter.AddListener(GetDrop);

        points = 0;
    }

    #region Static Methods

    public static void SetHealth(int value)
    {
        Health = value;
    }

    #endregion

    #region Input Callbacks

    public void Move(InputAction.CallbackContext context)
    {
        Vector2 value = context.ReadValue<Vector2>();
        horizontal = value.x;
        vertical = value.y;
    }

    public void Look(InputAction.CallbackContext context)
    {
        mousePosition = context.ReadValue<Vector2>();
    }

    public void Fire(InputAction.CallbackContext context)
    {
        lightParticle.Play();
        
    }

    public void Fire2(InputAction.CallbackContext context)
    {
        if (SecondShootAvailable)
        {
            heavyParticle.Play();
            points = LevelManager.Points;
        }
    }

    #endregion

    public void ApplyDamage(int damage)
    {
        Health -= damage;

        if (Health <= 0)
            GameManager.OnGameOver.Invoke();
    }
    private void BallSlam(Collider other)
    {
        if (horizontal == 0 && vertical == 0)
            return;

        if (other.CompareTag("Enemy"))
        {
            Enemy enemy = other.GetComponent<Enemy>();

            if (enemy.enemyData.ballSlamImmune)
                return;

            enemy.ApplyDamage(playerData.ballSlamDamage);
        }
    }

    private void GetDrop(Collider other)
    {
        if (!other.CompareTag("Drop"))
            return;

        Drop drop = other.GetComponent<Drop>();

        switch (drop.type)
        {
            case DropType.Health:
                ApplyDamage(-drop.value);
                break;
        }

        drop.gameObject.SetActive(false);
    }

    private void Update()
    {
        SecondShootAvailable = (LevelManager.Points - points > playerData.secondShootCost);

        mouseWorldPosition = mainCamera.ScreenToWorldPoint(new Vector3(mousePosition.x, mousePosition.y, 100));
        Ray ray = mainCamera.ScreenPointToRay(mousePosition);
        RaycastHit hit;

        if(Physics.Raycast(ray, out hit))
            mouseWorldPosition = hit.point;

        else
            mouseWorldPosition = new Vector3(mouseWorldPosition.x, lightGun.position.y, mouseWorldPosition.z);

        lightGun.LookAt(mouseWorldPosition);
        heavyGun.LookAt(mouseWorldPosition);

        Vector3 direction = new Vector3(horizontal, 0f, vertical).normalized;

        if(direction.magnitude > 0.1f)
        {
            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
            angle = Mathf.SmoothDampAngle(visual.eulerAngles.y, targetAngle, ref turnSmoothVelocity, playerData.turningSmooth);          
        }     
    }

    private void FixedUpdate()
    {
        rb.AddForce(new Vector3(horizontal, 0f, vertical) * playerData.motorStrength, ForceMode.Force);
        visual.transform.rotation = Quaternion.Euler(0f, angle, 0f);
        wheel.Rotate(Vector3.left * playerData.wheelSpeed * Time.deltaTime * rb.velocity.magnitude);
    }

}
