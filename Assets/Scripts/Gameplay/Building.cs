using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(BoxCollider))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(NavMeshObstacle))]
public class Building : MonoBehaviour
{
    [Header("Gameplay")]
    public int points;

    [Header("Visuals")]
    [SerializeField] private GameObject[] staticModels;
    [SerializeField] private GameObject[] dynamicModels;
    [SerializeField] private VFXType destroyVFX;
    [SerializeField] private Transform destroySpawn;

    private BoxCollider boxCollider;

    private void Awake()
    {
        boxCollider = GetComponent<BoxCollider>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player"))
            return;

        foreach (GameObject go in staticModels)
            go.SetActive(false);

        foreach (GameObject go in dynamicModels)
            go.SetActive(true);

        boxCollider.enabled = false;

        VFXManager.PlayVFX(destroyVFX, destroySpawn);
        LevelManager.AddPoints(points);

        StartCoroutine(DisableRigidbody());
    }

    private IEnumerator DisableRigidbody()
    {
        yield return new WaitForSeconds(10);

        foreach (GameObject go in dynamicModels)
        {
            Rigidbody rb = go.GetComponent<Rigidbody>();
            rb.isKinematic = true;
            rb.Sleep();
        }
    }
}
