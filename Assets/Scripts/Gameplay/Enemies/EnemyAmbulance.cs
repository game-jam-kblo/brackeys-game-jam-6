using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAmbulance : Enemy
{
    private Enemy enemyToHeal;
    private bool isAttacking;
    private float cachedTime;

    protected override void Update()
    {
        foreach (Enemy enemy in LevelManager.activeEnemies)
        {
            if (enemy.enemyData.Equals(enemyData))
                continue;

            if (!LevelManager.activeEnemies.Contains(enemyToHeal))
                enemyToHeal = enemy;

            if (enemyToHeal.life > enemy.life)
                enemyToHeal = enemy;
        }

        agent.SetDestination(enemyToHeal.gameObject.transform.position);

        if (Vector3.Distance(transform.position, enemyToHeal.transform.position) <= enemyData.attackDistance)
            Attack();
    }

    private void Attack()
    {
        if (!isAttacking)
        {
            cachedTime = Time.time;
            isAttacking = true;
        }

        float elapsedTime = Time.time - cachedTime;

        if (elapsedTime >= enemyData.fireRate)
        {
            Heal();
            cachedTime = Time.time;
        }
    }

    private void Heal()
    {
        if (enemyToHeal.enemyData.Equals(enemyData))
            return;

        enemyToHeal.ApplyDamage(-enemyData.fireDamage);
        VFXManager.PlayVFX(VFXType.Heal, enemyToHeal.transform);
    }
}
