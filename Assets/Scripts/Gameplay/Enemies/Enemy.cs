using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public abstract class Enemy : MonoBehaviour, IDamage
{
    public EnemyData enemyData;

    protected NavMeshAgent agent;
    protected Transform player;

    [HideInInspector] public int life;

    protected virtual void OnEnable()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.speed = enemyData.speed;
        agent.angularSpeed = enemyData.angularSpeed;
        agent.acceleration = enemyData.acceleration;
        agent.stoppingDistance = enemyData.chaseDistance;
        life = enemyData.life;
        player = Player.Transform;

        LevelManager.activeEnemies.Add(this);

        VFXManager.PlayVFX(enemyData.spawnEffect, transform);
    }

    protected virtual void OnDisable()
    {
        LevelManager.activeEnemies.Remove(this);
        LevelManager.inactiveEnemies.Add(this);
    }

    public virtual void ApplyDamage(int damage)
    {
        life -= damage;

        if (life > enemyData.life)
            life = enemyData.life;

        if (life <= 0)
            Die();
    }

    protected virtual void Die()
    {
        VFXManager.PlayVFX(enemyData.dieEffect, transform);

        if (enemyData.hasDrop && Random.value <= enemyData.dropChance)
            VFXManager.PlayVFX(enemyData.dropEffect, transform);

        FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/sfx_game_explosion");

        gameObject.SetActive(false);
    }

    protected virtual void Update()
    {

    }
}
