using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGun : Enemy
{
    [SerializeField] protected Transform gun;
    [SerializeField] protected ParticleSystem ps;
    [SerializeField] protected string sound;

    private bool isAttacking;
    private float cachedTime;

    protected override void OnEnable()
    {
        base.OnEnable();

        ps.GetComponent<ParticleCollision>().value = enemyData.fireDamage;
    }

    private void Attack()
    {
        if (!isAttacking)
        {
            cachedTime = Time.time;
            isAttacking = true;
        }

        float elapsedTime = Time.time - cachedTime;

        if (elapsedTime >= enemyData.fireRate)
        {
            Fire();
            cachedTime = Time.time;
        }
    }

    private void Fire()
    {
        ps.Play();
        FMODUnity.RuntimeManager.PlayOneShot(sound);

        if (enemyData.hasShootEffect)
            VFXManager.PlayVFX(enemyData.shootEffect, ps.transform);
    }

    protected override void Update()
    {
        agent.SetDestination(player.position);
        gun.LookAt(player.position);

        if (Vector3.Distance(transform.position, player.position) <= enemyData.attackDistance)
            Attack();
    }
}
