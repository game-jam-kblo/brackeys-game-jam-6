using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VFXPool : MonoBehaviour
{
    public VFXType Type { get; private set; }

    private Queue<GameObject> effects = new Queue<GameObject>();
    private VFXData data;

    public void SetPool(VFXData _data)
    {
        Type = _data.type;
        data = _data;

        for (int i = 0; i < data.amount; i++)
        {
            GameObject go = Instantiate(data.prefab, transform);
            go.SetActive(false);
            effects.Enqueue(go);
        }
    }

    public void ReleaseEffect(Transform transform)
    {
        StartCoroutine(PlayEffect(transform));
    }

    private IEnumerator PlayEffect(Transform transform)
    {
        GameObject go = effects.Dequeue();
        go.transform.SetPositionAndRotation(transform.position, transform.rotation);
        go.SetActive(true);

        yield return new WaitForSeconds(data.duration);

        go.SetActive(false);
        effects.Enqueue(go);
    }
}
