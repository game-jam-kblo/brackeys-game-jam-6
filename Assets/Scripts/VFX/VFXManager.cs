using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VFXManager : MonoBehaviour
{
    private static List<VFXPool> pools = new List<VFXPool>();

    [SerializeField] private VFXData[] datas;

    private void Awake()
    {
        pools.Clear();

        foreach (VFXData data in datas)
            VFXPool(data);                
    }

    private void VFXPool(VFXData data)
    {
        GameObject go = new GameObject(data.type.ToString());
        go.transform.SetParent(transform);

        VFXPool pool = go.AddComponent<VFXPool>();
        pool.SetPool(data);
        pools.Add(pool);
    }

    public static void PlayVFX(VFXType type, Transform transform)
    {
        VFXPool pool = pools.Find(x => x.Type.Equals(type));
        pool.ReleaseEffect(transform);
    }
}
