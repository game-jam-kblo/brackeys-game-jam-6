using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum VFXType
{
    Explosion,
    Puff,
    Heal,
    TankExplosion,
    Life,
    TankFlash,
    SmokeBuildings,
    SmokeBigBuildings
}

[CreateAssetMenu]
public class VFXData : ScriptableObject
{
    [Header("Visuals")]
    public GameObject prefab;
    public float duration;

    [Header("Pool Settings")]
    public VFXType type;
    public int amount;
}
