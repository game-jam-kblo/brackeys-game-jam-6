using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CameraShake : MonoBehaviour
{
    private CinemachineImpulseSource impulseSource;

    private static UnityEvent<float, float> shake = new UnityEvent<float, float>();

    private void Awake()
    {
        impulseSource = GetComponent<CinemachineImpulseSource>();

        shake.AddListener(SetShake);
    }

    private void OnDestroy()
    {
        shake.RemoveListener(SetShake);
    }

    public static void Shake(ShakeSettings shakeSettings)
    {
        shake.Invoke(shakeSettings.intensity, shakeSettings.duration);
    }

    private void SetShake(float intensity, float duration)
    {
        impulseSource.m_ImpulseDefinition.m_AmplitudeGain = intensity;
        impulseSource.m_ImpulseDefinition.m_TimeEnvelope.m_SustainTime = duration;
        impulseSource.GenerateImpulse();
    }
}

[System.Serializable]
public class ShakeSettings
{
    public float intensity;
    public float duration;
}
