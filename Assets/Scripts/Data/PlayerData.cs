using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData : ScriptableObject
{
    [Header("Movement")]
    public float motorStrength;
    public float mass;
    public float turningSmooth;

    [Tooltip("Only visuals")]
    public float wheelSpeed;

    [Header("Combat")]
    public int lightFireDamage;
    public int heavyFireDamage;
    public int ballSlamDamage;
    public int secondShootCost;
}
