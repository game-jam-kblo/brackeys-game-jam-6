using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class EnemyData : ScriptableObject
{
    [Header("Setup")]
    public GameObject prefab;

    [Header("Movement")]
    public float speed;
    public float angularSpeed;
    public float acceleration;

    [Header("Combat")]
    public int life;
    public int fireDamage;
    public int fireRate;
    public float chaseDistance;
    public float attackDistance;
    public bool ballSlamImmune;

    [Header("Drops")]
    public bool hasDrop;
    [Range(0, 1)] public float dropChance;

    [Header("Effects")]
    public VFXType spawnEffect;
    public VFXType dieEffect;
    public bool hasShootEffect;
    public VFXType shootEffect;
    public VFXType dropEffect;
}
