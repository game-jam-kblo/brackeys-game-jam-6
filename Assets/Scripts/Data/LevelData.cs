using UnityEngine;

[CreateAssetMenu]
public class LevelData : ScriptableObject
{
    [Header("Player")]
    public int playerHealth;

    [Header("Match Settings")]
    [Tooltip("In seconds")]
    public int matchDuration;
    public EnemyInfo[] enemyInfos;
}
